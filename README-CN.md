# 字符串操作

[![Netlify Status](https://api.netlify.com/api/v1/badges/c427f194-74aa-424c-b986-97c32f3bddee/deploy-status)](https://app.netlify.com/sites/easy-string-api/deploys)

一系列常用的字符串操作函数，用于 Fortran 。

**欢迎建议与代码贡献！**

## 使用 [Fortran-lang/fpm][1] 构建

```sh
fpm test    # 启动单元测试
```

[1]: https://github.com/fortran-lang/fpm

通过 fpm ，可以直接引用 easy-string 包。

```toml
[dependencies]
easy-string = { git = "https://gitee.com/fortran-stack/easy-string" }
```

## API 文档

```sh
ford FORD-project-file.md
```

最新的 API 文档，详见 [easy-string-api](https://easy-string-api.netlify.app/)。

## 参考链接

- [fortran-lang/stdlib](https://github.com/fortran-lang/stdlib)
- [fortran-lang/fpm](https://github.com/fortran-lang/fpm)
- [fortran-fans/forlab](https://github.com/fortran-fans/forlab)
- [everythingfunctional/strff](https://gitlab.com/everythingfunctional/strff/)
