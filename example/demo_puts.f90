program main

    use string_m
    call puts("Read a Integer: ")
    read (*, *) i
    print '(2a,i0)', "You entered: "//newline, "i = ", i

end program main
